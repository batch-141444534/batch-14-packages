<?php

namespace Pondit\Slider;

use Illuminate\Support\Facades\Route;
use Pondit\Slider\Http\controllers\SliderController;

Route::group(['middleware' => 'web'], function () {
    Route::get('/sliders', [SliderController::class, 'index'])->name('slide.index');
});

